/*
Bones Scripts File
Author: Eddie Machado

This file should contain any js scripts you want to add to the site.
Instead of calling it in the header or throwing it inside wp_head()
this file will be called automatically in the footer so as not to
slow the page load.

*/

// IE8 ployfill for GetComputed Style (for Responsive Script below)
if (!window.getComputedStyle) {
	window.getComputedStyle = function(el, pseudo) {
		this.el = el;
		this.getPropertyValue = function(prop) {
			var re = /(\-([a-z]){1})/g;
			if (prop == 'float') prop = 'styleFloat';
			if (re.test(prop)) {
				prop = prop.replace(re, function () {
					return arguments[2].toUpperCase();
				});
			}
			return el.currentStyle[prop] ? el.currentStyle[prop] : null;
		}
		return this;
	}
}


//  MENU FUNCTIONS

// flag to allow clicking
var clickAllowed = true;

jQuery(function($){	
	/*
	Responsive jQuery is a tricky thing.
	There's a bunch of different ways to handle
	it, so be sure to research and find the one
	that works for you best.
	*/
	
  
    // click function
	
	jQuery('#menu-button').on({
		click: function(e){	
			if(clickAllowed) {
				//e.preventDefault();
				e.stopPropagation();
				jQuery("#inner-header nav").slideToggle( "slow" );	
			}	
		}
	});
});
	
function formatMenu(){
	/* Prevent default link of parent li */
	jQuery('.header #inner-header ul.top-nav > li > a').click(function(e) {
		e.preventDefault();
		//do other stuff when a click happens
	});	
	
	/*Override Basic Menu Hover Actions*/
	
	jQuery('header #inner-header ul.top-nav li').children('ul').css("display","none");
	
	jQuery('header #inner-header ul.top-nav li').hover(function() {
		if (jQuery(this).children('ul').is(':visible')){ 
			jQuery( this ).children('ul').css("display","block");
		}
		else {
			jQuery( this ).children('ul').css("display","none");
		}	
	  }, function() {
			if (jQuery(this).children('ul').is(':visible')){ 
				jQuery(this).children('ul').css("display","block");
			}
			else {
				jQuery( this ).children('ul').css("display","none");
			}
	  }
	);
		
	
	/*Sequence New Drop Down Actions*/
	
	jQuery('header #inner-header ul.top-nav > li > a').on({
		click: function(e){	
		//console.log(e);
			if (this == e.target) {
				if (jQuery(this).siblings('ul').is(':visible')){ 
					jQuery( this ).siblings('ul').slideUp('medium');
					//console.log('A');
				}
				else if (jQuery(this).parent().siblings().children('.sub-menu').is(':visible')){ 
					
					//console.log('B');
					jQuery(this).parent().siblings().children('.sub-menu').filter(':visible').slideUp('medium');
					jQuery(this).siblings('ul').slideDown('medium');
				}
				else {
					//console.log('C');
					jQuery(this).siblings('ul').slideDown('medium');
				}
			}
		}
	});
}
	

// RESPONSIVE LAYOUT FUNCTIONS
	
function onResize() {
	
	// Responsive Images for page  & single 
	// formatResponsiveImages();
	
	if (!jQuery('body').hasClass('home')) {
		
		var flexW = jQuery(".entry-content").width();
		var flexH = (flexW/100)*66;
		var flexH2= (flexW/100)*44;
		
		// Archive
		jQuery(".entry-content .thumbnail-link.thumbnail-v1").css({"height":flexH,"overflow":"hidden"});
		// Exhibition & Boutique Archives
		jQuery(".entry-content .thumbnail-link.thumbnail-v2").css({"height":flexH,"overflow":"hidden"});
		// Pages & single
		jQuery(".entry-content.image-wrapper").css({"height":flexH2,"overflow":"hidden"});
	}
	
	//Define Window object for binding parallax & viewport width
	var $window = jQuery(window); 
	
	/* getting viewport width */
	var responsive_viewport = $window.width(); //jQuery(window).width();
	
	/* if is below 768px */
	if (responsive_viewport < 768) {
		//  Responsive Dropdown Menu
		clickAllowed = true;
		jQuery("#inner-header nav").css({"display":"none", "background":"#fff", "padding":"0 22px 22px", "position": "absolute", "top": "105px", "width": "100%", "z-index": "1000000"});		
	}
	
	/* if is above or equal to 768px */
	if (responsive_viewport >= 768) {
		
		// Responsive Dropdown Menu
		clickAllowed = false;
		jQuery("#inner-header nav").css({"display":"block", "position":"relative", "top":"0px", "width":"504px", "padding":"0px 0px 0px 0px", "margin":"0px auto"  });
		
	}
	
	/* if is less than 1030px */
	if (responsive_viewport < 1030) {
		
		if (jQuery('body').hasClass('home')) {
		
			// Disable Parallax
			// Check Bind
			$window.unbind('scroll', parallaxVariations);
			
			//Responsive Images
			
			//Resize Home Image Wrapper below 1030
			var hflexW = jQuery(".home article").width();
			var hflexH = (hflexW/100)*66;
			jQuery(".home article").css({"height":hflexH,"overflow":"hidden"});
			
			//Resize Home Background Images below 1030 (Resizing BG Pos here because the parallax may have changed it)
			var jflexCssSize= Math.ceil(hflexW) + 'px ' + Math.ceil(hflexH) + 'px';
			jQuery('.home article').css({'background-size':jflexCssSize, "background-attachment":"scroll", "backgroundPosition": "50% 0"});
		}
		
		// Responsive Sidebar
		jQuery('aside.entry-content').css({"width":"100%", "margin-top":"0px",  "padding-top":"0px"  }).insertAfter('section.entry-content #entry-subtitles');		
		jQuery("#entry-subtitles").height("auto");		
		jQuery("#mbal-social").height("auto");
	}
	
	/* Should be 1030 (css bug?) */
	if (responsive_viewport > 1014){
		//  Responsive Menu
		jQuery("#inner-header nav").css({ "width":"694px" });
	}
	
	/* off the bat large screen actions */
	if (responsive_viewport > 1030) {
	
		// Responsive Images
		if (jQuery('body').hasClass('home')) {
			// Reset Home Image Wrapper when we scale back up
			jQuery(".home article").css({"height":"752px","overflow":"hidden"});
			//jQuery('.home article').css({'background-size':'100% 752px', "background-attachment":"fixed", "backgroundPosition": "50% 109px"});
			jQuery('.home article').css({'background-size':'1368px 902px', "background-attachment":"fixed", "backgroundPosition": "50% 109px"});
			
		}
		
		jQuery(".entry-content .thumbnail-link.thumb-nail-v2").css({"height":flexH,"overflow":"hidden"});
		
		//Responsive Sidebar		
		jQuery('aside.entry-content').css({"width":"175px"}).insertAfter('section.entry-content.single-lower');		
		jQuery("#mbal-social").css({"padding-bottom":"10px"});
	
		var highestCol = Math.max(jQuery('#mbal-social').height(),jQuery('#entry-subtitles').height());
	
		jQuery("#entry-subtitles").height(highestCol);		
		jQuery("#mbal-social").height(highestCol);
		
	}

}


// PARALLAX AND SCROLLING FUNCTIONS

function parallaxVariations(){
	
	// The reason we bind the parallax variations to scroll
	// is so they can use the scroll top position
    // as a directional reference or anchor.
	
	var $window = jQuery(window); 
	var velocity = 0.15;
	var pos = $window.scrollTop(); 
	
	jQuery("body.home #main article").each(function(i) { 
		var $element = jQuery(this); 
		var height = $element.height(); 
		/*Trial and error. The 4 was subtracted just to tweek the value a little bit for layout purposes*/
		jQuery(this).css('backgroundPosition', '50% ' + (Math.round((height - pos) * velocity) + (i*109) - 4) +'px');	
		
		//console.log('h : (' + height + ') - p : (' + pos + ') * v  : (' + velocity + ') + (i*109) - 4 = ' +  (Math.round((height - pos) * velocity) + (i*109) -4)  +'px');	
	}); 
}


function activateScrollQueue(){ 
	
	var $window = jQuery(window); 
	var pos = $window.scrollTop(); 
	
	if (!pos==0){
		jQuery('html,body').delay(1000).animate({scrollTop: 0}, 1200); //.promise().done(function(){});
	}
	
	jQuery('body.home #main').children('article').first().addClass('current');
	
	
	// escape setInterval if user clicks or scrolls
	// Create large images and set bigger background size and velocity
	// Set the velocity of the text and fade
	
	// Note set interval and each is a strange way to loop since the two cycles run concurrently
	// See the two console.log things below
	
	t=setInterval(function(){
		jQuery('body.home #main').children('article').each(function( index ) { 
			
			var $cur = jQuery(this).removeClass('current');
			//console.log(index+" cnl: "+$cur.next().length);
			
			var $next = $cur.next().length?$cur.next():jQuery(this).siblings().eq(0);
			$next.addClass('current');
			blockOffset = $next.offset().top;
			jQuery('html,body').delay(3000).animate({scrollTop: blockOffset-109}, 2000).promise().done(function(){});
		});
	},5000);
	
	// Stop auto scroll when user interacts
	jQuery("html, body").bind("scroll mousedown DOMMouseScroll mousewheel keyup", function(){
		clearInterval(t);
		jQuery('html,body').stop(true,false);
	});
	
};

// Mask the homepage images

function footerResize(){

	var articleHeight = jQuery('.home article').height();
	var windowHeight = jQuery(window).height();
	var fHeight = windowHeight-(articleHeight+109);
	
	if (fHeight>0){
		jQuery('.footer').css('height',fHeight);
		jQuery('#inner-content').css('padding-bottom',fHeight);
	}
}		
	

// Resize Google Maps 	

function mapResizing() {
	console.log("Map");
   var mapOptions = {
           zoom: 15,
           center: new google.maps.LatLng(47.0585, 6.7497),  
           mapTypeId: google.maps.MapTypeId.ROADMAP
       };
 
   var map = new google.maps.Map(document.getElementById('responsive-google-map'),
                                   mapOptions);
                              
	var contentString = '<div id="map-info"><h3>Mus&eacute;e des beaux-arts</h3><p>Marie-Anne-Calame 6<br/>2400 Le Locle</p></div>'+
	'<h4><small><a style="text-align: left;" href="https://maps.google.fr/maps?f=q&amp;source=embed&amp;hl=fr&amp;geocode=&amp;q=Mus%C3%A9e+des+beaux-arts,+Le+Locle,+Suisse&amp;aq=0&amp;oq=mus%C3%A9e+des+beaux-arts+le+&amp;sll=46.75984,1.738281&amp;sspn=11.396553,9.997559&amp;ie=UTF8&amp;hq=Mus%C3%A9e+des+beaux-arts,&amp;hnear=Le+Locle,+Neuch%C3%A2tel,+Suisse&amp;t=m&amp;ll=47.0585,6.7497&amp;spn=0.006295,0.006295">Agrandir le plan</a></small></h4>';
	
	var infowindow = new google.maps.InfoWindow({
      content: contentString
	});
	
	var marker = new google.maps.Marker({
	   map: map,
	   draggable: false,
	   position: new google.maps.LatLng(47.0585, 6.7497)
    });
	
	google.maps.event.addListener(marker, 'click', function() {
		infowindow.open(map,marker);
	});
  
}
	
// DOCUMENT CONTROLLER

// as the page loads, call these scripts
jQuery(document).ready(function($) {	
	
	var $window = jQuery(window);
	var responsive_viewport = $window.width(); //jQuery(window).width();
	
	/*Responsive jQuery was here. Moved to window.load.resize.bind  below*/
							
	//Make Div clickable
	
		jQuery(".home #inner-content #main").css({"background":"#1B3F94"});
	
		$( ".home #inner-content #main article.home-feature" ).on({
		  mouseenter: function() {
			$( this ).css({"cursor": "pointer"});
		  },
		  mousedown: function() {
			$( this ).css({"zoom": "1", "filter": "alpha(opacity=45)", "opacity": "0.45"});
		  }, mouseup: function() {
			$( this ).css({"zoom": "1", "filter": "alpha(opacity=100)", "opacity": "1"});
		  }, click: function() {
			window.location=$(this).find("a").attr("href"); 
			 return false;
		  }
		});
		
	/* call once on the homepage */
		if ((responsive_viewport > 1030)&&(jQuery('body').hasClass('home'))) {
				
			// Bind Parrallax variables to scroll &  activate. 
			$window.bind('scroll', parallaxVariations);
			activateScrollQueue();
		
			// set footer size
			footerResize();
			$window.bind('resize', footerResize);
		}
		
	onResize();
	
	// Menu Formatting
	formatMenu();
	
	// Responsive Google Maps
	
	// Not in use yet. Jsut using the css part:
	// http://stackoverflow.com/questions/14370115/quick-and-easy-way-to-make-google-maps-iframe-embed-responsive
	if (jQuery('body').hasClass('page-id-61')) {
		mapResizing();                    
		google.maps.event.addDomListener(window, 'resize', mapResizing);
		google.maps.event.addDomListener(window, 'load', mapResizing);
	}
	 
}); /* end of as page load scripts */

jQuery(window).bind("resize",function() {	
	onResize();
});

/*! A fix for the iOS orientationchange zoom bug.
 Script by @scottjehl, rebound by @wilto.
 MIT License.
*/
(function(w){
	// This fix addresses an iOS bug, so return early if the UA claims it's something else.
	if( !( /iPhone|iPad|iPod/.test( navigator.platform ) && navigator.userAgent.indexOf( "AppleWebKit" ) > -1 ) ){ return; }
	var doc = w.document;
	if( !doc.querySelector ){ return; }
	var meta = doc.querySelector( "meta[name=viewport]" ),
		initialContent = meta && meta.getAttribute( "content" ),
		disabledZoom = initialContent + ",maximum-scale=1",
		enabledZoom = initialContent + ",maximum-scale=10",
		enabled = true,
		x, y, z, aig;
	if( !meta ){ return; }
	function restoreZoom(){
		meta.setAttribute( "content", enabledZoom );
		enabled = true; }
	function disableZoom(){
		meta.setAttribute( "content", disabledZoom );
		enabled = false; }
	function checkTilt( e ){
		aig = e.accelerationIncludingGravity;
		x = Math.abs( aig.x );
		y = Math.abs( aig.y );
		z = Math.abs( aig.z );
		// If portrait orientation and in one of the danger zones
		if( !w.orientation && ( x > 7 || ( ( z > 6 && y < 8 || z < 8 && y > 6 ) && x > 5 ) ) ){
			if( enabled ){ disableZoom(); } }
		else if( !enabled ){ restoreZoom(); } }
	w.addEventListener( "orientationchange", restoreZoom, false );
	w.addEventListener( "devicemotion", checkTilt, false );
})( this );