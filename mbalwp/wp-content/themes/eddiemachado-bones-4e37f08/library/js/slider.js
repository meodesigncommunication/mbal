window.$ = jQuery;

$(function(){
	
	var $slides = $("#top-slider .slide");   
	var $bullets = $("#slider-bullet .bullet");   
	var $container = $("#top-slider");        
 	var currentSlide = 0;	
	var stayTime = 6;	
	var slideTime = 0.5; //backgroundColor
        
        var tl = new TimelineMax();
				
	TweenLite.set($slides.filter(":gt(0)"), {autoAlpha:0});
	TweenLite.set($bullets.filter(":gt(0)"), {backgroundColor:"#8d9fc9"});
	

	TweenLite.delayedCall(stayTime, nextSlide);
        
        $(document).on('contextmenu', function() {
            return false;
        });
        $(document).ready(function() {
            $("img").on("contextmenu",function(){
               return false;
            }); 
        }); 
				
	// Slider fade in / fade out
	function nextSlide()
        {	
            TweenLite.to($("#top-slider .slide").eq(currentSlide), slideTime, {autoAlpha:0} );	
            TweenLite.set($("#slider-bullet .bullet").eq(currentSlide), {backgroundColor:"#8d9fc9"});

            currentSlide = ++currentSlide % $slides.length;

            TweenLite.to($("#top-slider .slide").eq(currentSlide), slideTime, {autoAlpha:1} );
            TweenLite.set($("#slider-bullet .bullet").eq(currentSlide), {backgroundColor:"#1b3f94"});

            TweenLite.delayedCall(stayTime, nextSlide);	
	}
	
	$('.bullet').click(function(){ 
            console.log(currentSlide);
            nextSlideBullet($(this).attr('data-slide'));
	});

	function nextSlideBullet(nextSlideToShow)
        {   
            TweenLite.to( $("#top-slider .slide").eq(currentSlide), slideTime, {autoAlpha:0} );	
            TweenLite.set($("#slider-bullet .bullet").eq(currentSlide), {backgroundColor:"#8d9fc9"});
            
            currentSlide = nextSlideToShow;
            
            TweenLite.to( $("#top-slider .slide").eq(currentSlide), slideTime, {autoAlpha:1} );
            TweenLite.set($("#slider-bullet .bullet").eq(currentSlide), {backgroundColor:"#1b3f94"});
	}

});