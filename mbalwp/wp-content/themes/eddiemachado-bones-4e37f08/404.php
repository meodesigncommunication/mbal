<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap clearfix">

					<div id="main" class="eightcol first clearfix" role="main">

						<article id="post-not-found" class="hentry clearfix">

							<header class="article-header">

								<h1>Erreur 404 - Page introuvable</h1>

							</header>

							<section class="entry-content">

								<p>La page que vous cherchez est introuvable.</p>

							</section>

							<section class="search">

									<p><?php get_search_form(); ?></p>

							</section>

							<footer class="article-footer">

							</footer>

						</article>

					</div>

				</div>

			</div>

<?php get_footer(); ?>
