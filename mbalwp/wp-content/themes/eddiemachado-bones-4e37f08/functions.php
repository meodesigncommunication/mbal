<?php
/*
Author: Eddie Machado
URL: htp://themble.com/bones/

This is where you can drop your custom functions or
just edit things like thumbnail sizes, header images,
sidebars, comments, ect.
*/

/************* INCLUDE NEEDED FILES ***************/

/*
1. library/bones.php
	- head cleanup (remove rsd, uri links, junk css, ect)
	- enqueueing scripts & styles
	- theme support functions
	- custom menu output & fallbacks
	- related post function
	- page-navi function
	- removing <p> from around images
	- customizing the post excerpt
	- custom google+ integration
	- adding custom fields to user profiles
*/

function aol_init_session()
{
	if (!session_id())
		session_start();
	
}

add_action('init', 'aol_init_session', 1);


require_once( 'library/bones.php' ); // if you remove this, bones will break
/*
2. library/custom-post-type.php
	- an example custom post type
	- example custom taxonomy (like categories)
	- example custom taxonomy (like tags)
*/
require_once( 'library/custom-post-type.php' ); // you can disable this if you like
/*
3. library/admin.php
	- removing some default WordPress dashboard widgets
	- an example custom dashboard widget
	- adding custom login css
	- changing text in footer of admin
*/
// require_once( 'library/admin.php' ); // this comes turned off by default
/*
4. library/translation/translation.php
	- adding support for other languages
*/
// require_once( 'library/translation/translation.php' ); // this comes turned off by default

/*
4. library/translation/translation.php
	- adding support for other languages
*/


/************* THUMBNAIL SIZE OPTIONS *************/

// Thumbnail sizes
add_image_size( 'large-hard-crop', 1140, 752, true );
add_image_size( 'medium-hard-crop', 684, 452, true );
add_image_size( 'small-hard-crop', 374, 247, true );

add_image_size( 'single-medium-hard-crop', 684, 301, true );
add_image_size( 'single-small-hard-crop', 374, 165, true ); 



/*
to add more sizes, simply copy a line from above
and change the dimensions & name. As long as you
upload a "featured image" as large as the biggest
set width or height, all the other sizes will be
auto-cropped.

To call a different size, simply change the text
inside the thumbnail function.

For example, to call the 300 x 300 sized image,
we would use the function:
<?php the_post_thumbnail( 'bones-thumb-300' ); ?>
for the 600 x 100 image:
<?php the_post_thumbnail( 'bones-thumb-600' ); ?>

You can change the names and dimensions to whatever
you like. Enjoy!
*/

/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function bones_register_sidebars() {
	register_sidebar(array(
		'id' => 'sidebar1',
		'name' => __( 'Sidebar 1', 'bonestheme' ),
		'description' => __( 'The first (primary) sidebar.', 'bonestheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	/*
	to add more sidebars or widgetized areas, just copy
	and edit the above sidebar code. In order to call
	your new sidebar just use the following code:

	Just change the name to whatever your new
	sidebar's id is, for example:

	register_sidebar(array(
		'id' => 'sidebar2',
		'name' => __( 'Sidebar 2', 'bonestheme' ),
		'description' => __( 'The second (secondary) sidebar.', 'bonestheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	To call the sidebar in your template, you can just copy
	the sidebar.php file and rename it to your sidebar's name.
	So using the above example, it would be:
	sidebar-sidebar2.php

	*/
} // don't remove this bracket!


/************* SEARCH FORM LAYOUT *****************/

// Search Form


/* 
 * Removed this :
 * <label class="screen-reader-text" for="s">' . __( 'Search for:', 'bonestheme' ) . '</label> 
 *
 */ 

/********* MBAL IMAGE FALLBACK FUNCTION **********/

function imageCropFallback(){


	$the_thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "single-medium-hard-crop" );
	$the_thumbnail_src2 = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "single-small-hard-crop" );
	$thumbnail_src = $the_thumbnail_src[0];
	$thumbnail_src2 = $the_thumbnail_src2[0];
	
	//change this to exact WxH of your custom image size
	$check_thumb = strpos($thumbnail_src, '684x301'); 
	$check_thumb2 = strpos($thumbnail_src2, '374x165'); 
	
	// if custom image size exist
	if( $check_thumb ):
		the_post_thumbnail('single-medium-hard-crop'); 
	elseif( $check_thumb2 ):
		the_post_thumbnail('single-small-hard-crop'); 
	else:
		echo the_post_thumbnail('thumbnail'); // if not, use default thumbnail
	endif;
	

} 
 
 
 
function bones_wpsearch($form) {
	$form = '<form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
	<input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="' . esc_attr__( 'Rechercher', 'bonestheme' ) . '" />
	<input type="submit" id="searchsubmit" value="' . esc_attr__( 'Search' ) .'" />
	</form>';
	return $form;
} // don't remove this bracket!

/*-----------------------------------------------------------------------------------*/
/* Get The Template Name */
/*-----------------------------------------------------------------------------------*/

add_filter( 'template_include', 'var_template_include', 1000 );
function var_template_include( $t ){
    $GLOBALS['current_theme_template'] = basename($t);
    return $t;
}

function get_current_template( $echo = false ) {
    if( !isset( $GLOBALS['current_theme_template'] ) )
        return false;
    if( $echo )
        echo $GLOBALS['current_theme_template'];
    else
        return $GLOBALS['current_theme_template'];
}

/*-----------------------------------------------------------------------------------*/

add_filter('show_admin_bar', '__return_false');
add_filter('the_content', 'wpautop');

?>
