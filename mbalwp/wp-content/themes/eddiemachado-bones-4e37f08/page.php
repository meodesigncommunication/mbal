<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap clearfix">

						<div id="main" class="mbal-centre clearfix" role="main">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class( 'clearfix' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

								<header class="article-header">
									<h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>
								</header>
                                                                
                                                                <!-- SLIDER TOP PAGE -->
                                                                    <?php if( have_rows('slider') ): ?>
                                                                        <?php $count = 0; ?>
                                                                        <section id="top-slider">
                                                                            <div id="slide-container">
                                                                                <?php while( have_rows('slider') ): the_row(); 

                                                                                $image = (is_array(get_sub_field('images'))) ? get_sub_field('images') : array();
                                                                                $legende = (get_sub_field('legende') != '') ? get_sub_field('legende') : '';

                                                                                echo '<article data-count="'.$count.'" class="slide slide-'.$count.'"><a href="'.$image['url'].'" class="fancybox" rel="gallery" title="'.$legende.'"><img class="image-slide" src="'.$image['url'].'" alt="'.$legende.'" /></a></article>';

                                                                                $count++;

                                                                                endwhile; ?>
                                                                            </div>
                                                                            <section id="slider-bullet" style="width:<?php echo $count*15 ?>px">
                                                                                <?php for($i=0;$i<$count;$i++): ?>
                                                                                    <span data-slide="<?php echo $i ?>" class="bullet">&nbsp;</span>                                                            
                                                                                <?php endfor; ?>
                                                                            </section>
                                                                        </section>
                                                                    <?php else: ?>
                                                                        <?php if ( has_post_thumbnail() ) { ?>
                                                                            <section class="entry-content image-wrapper clearfix" itemprop="articleBody">
                                                                                <a title="<?php the_title_attribute(); ?>">
                                                                                        <?php imageCropFallback(); ?>
                                                                                </a>
                                                                            </section>
                                                                        <?php } ?>
                                                                    <?php endif; ?>
                                                                <!-- /SLIDER TOP PAGE --> 
								

																
								<section class="entry-content single-lower clearfix" itemprop="articleBody">
									<!-- if subtitle -->
									<?php 
										$subtitle = get_field('subtitle'); 
										
										//if($subtitle){									
									?>
									
										<div id="entry-subtitles">	
											<h2 class="h1" itemprop="headline"><?php echo $subtitle; ?></h2>
										</div>
									
									<?php  //} //endif ?>	
									
									<?php the_content(); ?>					
								</section>
								
								<!--<aside id="mbal-quote" class="entry-content single-lower clearfix">		
									<ul id="mbal-social" class="clearfix"></ul>
									<?php //$quote = get_field('single_quote'); ?>	
									<div id="mbal-quote"><h3><?php //echo $quote; ?></h3></div>
								</aside>-->
							
							</article>

								<?php endwhile; else : ?>

									<article id="post-not-found" class="hentry clearfix">
										<header class="article-header">
											<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
										</header>
										<section class="entry-content">
											<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
										</section>
										<footer class="article-footer">
											<p><?php _e( 'This is the error message in the page.php template.', 'bonestheme' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>

						</div>

						</div>

			</div>

<?php get_footer(); ?>
