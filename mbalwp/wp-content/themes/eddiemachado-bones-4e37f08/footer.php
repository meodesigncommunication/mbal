			<footer class="footer" role="contentinfo">
			
				<div id="inner-footer" class="wrap clearfix">
					<div class="mbal-centre clearfix">
					<?php  //echo "Template: <br/>".get_current_template();  ?>
					
					<nav role="navigation">
							<?php bones_footer_links(); ?>
					</nav>

					<!-- <p class="source-org copyright">&copy; <?php //echo date('Y'); ?> <?php //bloginfo( 'name' ); ?>.</p> -->
					</div>
				</div>

			</footer>

		</div>

		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>
		<script src="<?php echo get_template_directory_uri(); ?>/library/js/slider.js"></script>
	</body>

</html>
