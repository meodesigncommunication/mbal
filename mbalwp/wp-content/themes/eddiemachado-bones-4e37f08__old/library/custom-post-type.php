<?php
/* Bones Custom Post Type Example
This page walks you through creating 
a custom post type and taxonomies. You
can edit this one or copy the following code 
to create another one. 

I put this in a separate file so as to 
keep it organized. I find it easier to edit
and change things if they are concentrated
in their own file.

Developed by: Eddie Machado
URL: http://themble.com/bones/
*/

// Flush rewrite rules for custom post types
add_action( 'after_switch_theme', 'bones_flush_rewrite_rules' );

// Flush your rewrite rules
function bones_flush_rewrite_rules() {
	flush_rewrite_rules();
}

/**
  * MBAL CPT : Exhibitions 
  *
  **/
	
// let's create the function for the custom type
function custom_post_exhibitions() { 
	// creating (registering) the custom type 
	register_post_type( 'mbal_exhibitions', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Expositions', 'bonestheme' ), /* This is the Title of the Group */
			'singular_name' => __( 'Exposition', 'bonestheme' ), /* This is the individual type */
			'all_items' => __( 'All Expositions', 'bonestheme' ), /* the all items menu item */
			'add_new' => __( 'Add New', 'bonestheme' ), /* The add new menu item */
			'add_new_item' => __( 'Add New Exposition', 'bonestheme' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'bonestheme' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Expositions', 'bonestheme' ), /* Edit Display Title */
			'new_item' => __( 'New Exposition', 'bonestheme' ), /* New Display Title */
			'view_item' => __( 'View Exposition', 'bonestheme' ), /* View Display Title */
			'search_items' => __( 'Search Exposition', 'bonestheme' ), /* Search Custom Type Title */ 
			'not_found' =>  __( 'Nothing found in the Database.', 'bonestheme' ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'Nothing found in Trash', 'bonestheme' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'This is the Exposition post type', 'bonestheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 21, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => get_stylesheet_directory_uri() . '/library/images/custom-post-icon.png', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'exposition', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'exposition', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'custom-fields', 'revisions')
		) /* end of options */
	); /* end of register post type */
	
	/*
	// this adds your post categories to your custom post type 
	register_taxonomy_for_object_type( 'category', 'custom_type' );
	// this adds your post tags to your custom post type 
	register_taxonomy_for_object_type( 'post_tag', 'custom_type' );
	*/
}

	// adding the function to the Wordpress init
	add_action( 'init', 'custom_post_exhibitions');
	
	/*
	for more information on taxonomies, go here:
	http://codex.wordpress.org/Function_Reference/register_taxonomy
	*/
	
	// now let's add custom categories (these act like categories)
	register_taxonomy( 'mbal_exhibition_cat', 
		array('mbal_exhibitions'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
		array('hierarchical' => true,     /* if this is true, it acts like categories */
			'labels' => array(
				'name' => __( 'Exposition Categories', 'bonestheme' ), /* name of the custom taxonomy */
				'singular_name' => __( 'Exposition Category', 'bonestheme' ), /* single taxonomy name */
				'search_items' =>  __( 'Search Exposition Categories', 'bonestheme' ), /* search title for taxomony */
				'all_items' => __( 'All Exposition Categories', 'bonestheme' ), /* all title for taxonomies */
				'parent_item' => __( 'Parent Exposition Category', 'bonestheme' ), /* parent title for taxonomy */
				'parent_item_colon' => __( 'Parent Exposition Category:', 'bonestheme' ), /* parent taxonomy title */
				'edit_item' => __( 'Edit Exposition Category', 'bonestheme' ), /* edit custom taxonomy title */
				'update_item' => __( 'Update Exposition Category', 'bonestheme' ), /* update title for taxonomy */
				'add_new_item' => __( 'Add New Exposition Category', 'bonestheme' ), /* add new title for taxonomy */
				'new_item_name' => __( 'New Exposition Category Name', 'bonestheme' ) /* name title for taxonomy */
			),
			'show_admin_column' => true, 
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'exposition-categorie' ),
		)
	);
	
	
/**
  * MBAL CPT : Boutique 
  *
  **/
	
// let's create the function for the custom type
function custom_post_boutiques() { 
	// creating (registering) the custom type 
	register_post_type( 'mbal_boutiques', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		array( 'labels' => array(
			'name' => __( 'Boutiques', 'bonestheme' ), /* This is the Title of the Group */
			'singular_name' => __( 'Boutique', 'bonestheme' ), /* This is the individual type */
			'all_items' => __( 'All Boutiques', 'bonestheme' ), /* the all items menu item */
			'add_new' => __( 'Add New', 'bonestheme' ), /* The add new menu item */
			'add_new_item' => __( 'Add New Boutique', 'bonestheme' ), /* Add New Display Title */
			'edit' => __( 'Edit', 'bonestheme' ), /* Edit Dialog */
			'edit_item' => __( 'Edit Boutiques', 'bonestheme' ), /* Edit Display Title */
			'new_item' => __( 'New Boutique', 'bonestheme' ), /* New Display Title */
			'view_item' => __( 'View Boutique', 'bonestheme' ), /* View Display Title */
			'search_items' => __( 'Search Boutique', 'bonestheme' ), /* Search Custom Type Title */ 
			'not_found' =>  __( 'Nothing found in the Database.', 'bonestheme' ), /* This displays if there are no entries yet */ 
			'not_found_in_trash' => __( 'Nothing found in Trash', 'bonestheme' ), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'This is the Boutique post type', 'bonestheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 20, /* this is what order you want it to appear in on the left hand side menu */ 
			'menu_icon' => get_stylesheet_directory_uri() . '/library/images/custom-post-icon.png', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'boutique', 'with_front' => false ), /* you can specify its url slug */
			'has_archive' => 'boutiques', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'custom-fields', 'revisions')
		) /* end of options */
	); /* end of register post type */
	
	/*
	// this adds your post categories to your custom post type 
	register_taxonomy_for_object_type( 'category', 'custom_type' );
	// this adds your post tags to your custom post type 
	register_taxonomy_for_object_type( 'post_tag', 'custom_type' );
	*/
}

	// adding the function to the Wordpress init
	add_action( 'init', 'custom_post_boutiques');
	
	/*
	for more information on taxonomies, go here:
	http://codex.wordpress.org/Function_Reference/register_taxonomy
	*/

	
	// now let's add custom tags (these act like categories)
	register_taxonomy( 'mbal_boutique_tag', 
		array('mbal_boutiques'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
		array('hierarchical' => false,    /* if this is false, it acts like tags */
			'labels' => array(
				'name' => __( 'Boutique Categories', 'bonestheme' ), /* name of the custom taxonomy */
				'singular_name' => __( 'Boutique Category', 'bonestheme' ), /* single taxonomy name */
				'search_items' =>  __( 'Search Boutique Categories', 'bonestheme' ), /* search title for taxomony */
				'all_items' => __( 'All Boutique Categories', 'bonestheme' ), /* all title for taxonomies */
				'parent_item' => __( 'Parent Boutique Category', 'bonestheme' ), /* parent title for taxonomy */
				'parent_item_colon' => __( 'Parent Boutique Category:', 'bonestheme' ), /* parent taxonomy title */
				'edit_item' => __( 'Edit Boutique Category', 'bonestheme' ), /* edit custom taxonomy title */
				'update_item' => __( 'Update Boutique Category', 'bonestheme' ), /* update title for taxonomy */
				'add_new_item' => __( 'Add New Boutique Category', 'bonestheme' ), /* add new title for taxonomy */
				'new_item_name' => __( 'New Boutique Category Name', 'bonestheme' ) /* name title for taxonomy */
			),
			'show_admin_column' => true,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'boutique-categorie' ),
		)
	);	
?>
