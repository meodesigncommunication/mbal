<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap clearfix">

					<div id="main" class="mbal-centre clearfix" role="main">

						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" >

								<header class="article-header">
									<h1 class="entry-title single-title" itemprop="headline"><?php the_title(); ?></h1>
									
									<span class="prev-link">
										<?php //previous_post_link('<strong>%link</strong>') 
										previous_post_link_plus( array(
											'post_type' => '',
											'loop' => true,
											'in_same_cat' => true,
											'in_same_tax' => false,
											'format' => '%link',
											'link' => '%title'
											) );?>
									</span>
									<span class="next-link">
										<?php //next_post_link('<strong>%link</strong>') 
										next_post_link_plus( array(
											'post_type' => '',
											'loop' => true,
											'in_same_cat' => true,
											'in_same_tax' => false,
											'format' => '%link',
											'link' => '%title'
											) );?>
									</span>		
									
								</header>
								
								
								<section class="entry-content image-wrapper clearfix" itemprop="articleBody">
									<a title="<?php the_title_attribute(); ?>">
										<?php imageCropFallback(); ?>
									</a>
								</section>	
								
								<section class="entry-content single-lower clearfix" itemprop="articleBody">
									
									<?php
										$subtitle = get_field('subtitle'); 
										$startdate = get_field('start_date');
										$enddate = get_field('end_date');
										
										$format = "G\hi";
										$starttimestamp = get_field('start_time');
										$endtimestamp = get_field('end_time');
										
													
										//print_r($starttimestamp); 
										//echo $endtimestamp;
										
										//echo "<hr/>";
										
										//For Pinterest
										$thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "large" ); 
									?>
									
									
									
									<div id="entry-subtitles">
										<h2 class="h1" itemprop="headline"><?php echo $subtitle; ?></h2>
										
										<?php if($startdate){ ?>
											<h1 class="datetitle">
											<?php 
											echo $startdate; 
											if(!$enddate==""){
												echo "&#8722;".$enddate; 
											}
											?>
											</h1>
										<?php } //endif ?>	
										
										<?php  if($starttimestamp){ ?>
											<br/>
											<h2 class="timetitle">
											<?php 
											echo date_i18n( $format, $starttimestamp ); 
											
											if(!$endtimestamp==""){
												echo "&#8722;".date_i18n( $format, $endtimestamp ); 
											}
											?>
											</h2>
										<?php }  //endif ?>	
									
									</div>
									
									<?php the_content(); ?>					
								</section>
								
								<aside class="entry-content single-lower clearfix">
									
									<ul id="mbal-social" class="clearfix">
										
										<li id="pinterest-icon"><a href="http://pinterest.com/pin/create/bookmarklet/?media=<?php echo $thumbnail_src[0]; ?>&url=<?php the_permalink(); ?>&is_video=false&description=<?php the_title(); ?>" target="_blank">P</a></li>
										<li id="facebook-icon"><a href="http://www.facebook.com/share.php?u=<?php the_permalink(); ?>&title=<?php the_title(); ?>" target="_blank">FB</a></li>
										<li id="twitter-icon"><a href="http://twitter.com/home?status=<?php the_permalink(); ?>+<?php the_title(); ?>" target="_blank">Tw</a></li>
										<li id="t-icon"><a href="http://www.tumblr.com/share?v=3&u=<?php echo urlencode(get_permalink()); ?>&t=<?php echo urlencode(get_the_title());?>" target="_blank">T</a></li>
									</ul>
									
									<?php $quote = get_field('single_quote'); ?>	
									
									<div id="mbal-quote"><h3><?php echo $quote; ?></h3></div>
																		
								</aside>		
								
								<div class="clearfix"></div>
								
								<section class="entry-content entry-gallery clearfix" itemprop="articleBody">
								
								<?php
								
								$images = get_field('gallery'); 
								
								if( $images ):  	
								
									foreach( $images as $image ):
																		
										$imageSizes=$image['sizes'];
                                                                                $size=$imageSizes['large'];
										
								?>
								
									<img src="<?php echo $size; ?>" alt="<?php echo $image['alt']; ?>"/>
									<p class="image-caption"><?php echo $image['caption']; ?></p>
								<?php endforeach;

								endif;    ?>
								
								</section>

								<footer class="article-footer">
									<?php //the_tags( '<p class="tags"><span class="tags-title">' . __( 'Tags:', 'bonestheme' ) . '</span> ', ', ', '</p>' ); ?>
								</footer>

							</article>

						<?php endwhile; ?>

						<?php else : ?>

							<article id="post-not-found" class="hentry clearfix">
									<header class="article-header">
										<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
									</header>
									<section class="entry-content">
										<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
									</section>
									<footer class="article-footer">
											<p><?php _e( 'This is the error message in the single.php template.', 'bonestheme' ); ?></p>
									</footer>
							</article>

						<?php endif; ?>

					</div>

				</div>

			</div>

<?php get_footer(); ?>
