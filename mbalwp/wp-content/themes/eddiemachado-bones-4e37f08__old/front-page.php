<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap clearfix">

						<div id="main" class="twelvecol first parallax-wrapper clearfix" role="main">
							
							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
							
							<article id="post-<?php the_ID(); ?>" <?php post_class( 'home-logo clearfix' ); ?> role="article" data-stellar-background-ratio="0.5" style="background-image: url('<?php bloginfo('template_directory'); ?>/library/images/entree-site.jpg');">
									
								<section class="article-header">
									&nbsp;
								</section>								
									
							</article>
								
							<? $posts = get_field('homepage_features');	
							if( $posts ){
							
								foreach( $posts as $post): // variable must be called $post (IMPORTANT) 
								setup_postdata($post);
							
								$the_thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "large-hard-crop" );//
								$thumbnail_src = $the_thumbnail_src[0];
								
								/* What field if no dates ? */
								$startdate = get_field('start_date');
								$enddate = get_field('end_date');
								
								?>
								
								<article id="post-<?php the_ID(); ?>" <?php post_class( 'home-feature clearfix' ); ?> role="article" style="background-image: url(<?php echo $thumbnail_src;?>);" data-stellar-background-ratio="0.5">  <!--data-stellar-vertical-offset="<?php //echo $i*109; ?>"--> 
									
									<header class="article-header"><!--data-stellar-ratio="1"-->
										<h1 class="page-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h1>
									</header>
									
									<footer class="article-footer" ><!--data-stellar-ratio="1"-->
										<h1><a href="<?php the_permalink();?>" class="datetitle" rel="bookmark" title="<?php the_title_attribute();?>"><?php echo $startdate; ?></a></h1>
										<h1><a href="<?php the_permalink();?>" class="datetitle" rel="bookmark" title="<?php the_title_attribute();?>"><?php echo $enddate; ?></a></h1>
									</footer>
									
								</article>
								
								<?php
								endforeach;
								
								wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly 
							} 
							?>
							
							<?php endwhile; else : ?>
							
							<article id="post-not-found" class="hentry clearfix">
								<header class="article-header">
										<h1><?php _e( 'MBAL', 'bonestheme' ); ?></h1>
								</header>
								
								<footer class="article-footer">
										<h1><?php _e( 'No Posts selected', 'bonestheme' ); ?></h1>
								</footer>
							</article>
							
							<?php endif; ?>
					</div>
				</div>
			</div>
<?php get_footer(); ?>