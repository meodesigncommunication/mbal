<?php
/*
This is the custom post type taxonomy template.
If you edit the custom taxonomy name, you've got
to change the name of this template to
reflect that name change.

i.e. if your custom taxonomy is called
register_taxonomy( 'shoes',
then your single template should be
taxonomy-shoes.php

*/

?>

<?php get_header(); ?>

			<div id="content" class="exhibition-agenda-tpl">

				<div id="inner-content" class="wrap clearfix">

						<div id="main" class="mbal-centre clearfix" role="main">

							<h1 class="archive-title"><?php single_cat_title(); ?></h1>

							<?php 
							$printedPast = false;
							$hasCurrent = false;
							$mktoday = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
							
							if (have_posts()) : while (have_posts()) : the_post();
	
								//global $post;
								//$startdate = get_post_meta( $post->ID, '_mbal_startdate', true );
								//$enddate = get_post_meta( $post->ID, '_mbal_enddate', true );
								$startdate = get_field('start_date');
								$enddate = get_field('end_date');
								
								// Single day event?
								$dateToCompare = $enddate;
								if($dateToCompare == '' || empty($dateToCompare)){
									$dateToCompare = $startdate;
								}
								echo '<!-- '.$dateToCompare.' -->';
								// Check for past events
								$explodeDate = explode('.', $dateToCompare);
								$mkdateToCompare = mktime(0, 0, 0, $explodeDate[1], $explodeDate[0], ($explodeDate[2]+2000));
								
								if($mktoday < $mkdateToCompare && !$hasCurrent) {
									$hasCurrent = true;
								}
								
								if($mktoday > $mkdateToCompare && $hasCurrent && !$printedPast) {
									echo '</div>
												<div role="main" class="mbal-centre clearfix">
												<h2 class="archive-title agenda-past-events">&Eacute;v&egrave;nements pass&eacute;s</h2>';
									$printedPast = true;
								}
							?>

							<article id="post-<?php the_ID(); ?>" <?php post_class( 'clearfix' ); ?> role="article">

								<header class="article-header">
									<h1 class="h2">
										<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
										<?php the_title(); ?>
										</a>
									</h1>
								</header>

								<section class="entry-content clearfix" itemprop="articleBody">
									<a href="<?php the_permalink() ?>" rel="bookmark" class="thumbnail-link thumbnail-v2" title="<?php the_title_attribute(); ?>">
											<?php 	
												
												$the_thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "small-hard-crop" );
												$thumbnail_src = $the_thumbnail_src[0];
												//change this to exact WxH of your custom image size
												$check_thumb = strpos($thumbnail_src, '374x247'); 

												// if custom image size exist
												if( $check_thumb ):
												the_post_thumbnail('small-hard-crop'); 
												else:
												echo the_post_thumbnail('thumbnail'); // if not, use default thumbnail
												endif;
										?>
									</a>
								</section>

								<footer class="article-footer">
									
									<h1><a href="<?php the_permalink();?>" rel="bookmark" title="<?php the_title_attribute();?>"><?php echo $startdate; ?></a></h1>
									<h1><a href="<?php the_permalink();?>" rel="bookmark" title="<?php the_title_attribute();?>"><?php echo $enddate; ?></a></h1>
	
								</footer>

							</article>

							<?php endwhile; ?>

									<?php if ( function_exists( 'bones_page_navi' ) ) { ?>
											<?php bones_page_navi(); ?>
									<?php } else { ?>
											<nav class="wp-prev-next">
													<ul class="clearfix">
														<li class="prev-link"><?php next_posts_link( __( '&laquo; Older Entries', 'bonestheme' )) ?></li>
														<li class="next-link"><?php previous_posts_link( __( 'Newer Entries &raquo;', 'bonestheme' )) ?></li>
													</ul>
											</nav>
									<?php } ?>

							<?php else : ?>

									<article id="post-not-found" class="hentry clearfix">
										<header class="article-header">
											<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
										</header>
										<section class="entry-content">
											<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the taxonomy-custom_cat.php template.', 'bonestheme' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>

						</div>
				</div>

			</div>

<?php get_footer(); ?>
