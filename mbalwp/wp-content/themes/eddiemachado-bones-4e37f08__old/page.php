<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap clearfix">

						<div id="main" class="mbal-centre clearfix" role="main">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class( 'clearfix' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

								<header class="article-header">
									<h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>
								</header>
								
								<!-- if thumbnail -->
								
								<?php
									// Must be inside a loop.

									if ( has_post_thumbnail() ) { ?>
										<section class="entry-content image-wrapper clearfix" itemprop="articleBody">
											<a title="<?php the_title_attribute(); ?>">
											<?php imageCropFallback(); ?>
											</a>
										</section>										
								<?php } ?>
								

																
								<section class="entry-content single-lower clearfix" itemprop="articleBody">
									<!-- if subtitle -->
									<?php 
										$subtitle = get_field('subtitle'); 
										
										//if($subtitle){									
									?>
									
										<div id="entry-subtitles">	
											<h2 class="h1" itemprop="headline"><?php echo $subtitle; ?></h2>
										</div>
									
									<?php  //} //endif ?>	
									
									<?php the_content(); ?>					
								</section>
								
								<aside id="mbal-quote" class="entry-content single-lower clearfix">		
									<ul id="mbal-social" class="clearfix"></ul>
									<?php $quote = get_field('single_quote'); ?>	
									<div id="mbal-quote"><h3><?php echo $quote; ?></h3></div>
								</aside>
							
							</article>

								<?php endwhile; else : ?>

									<article id="post-not-found" class="hentry clearfix">
										<header class="article-header">
											<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
										</header>
										<section class="entry-content">
											<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
										</section>
										<footer class="article-footer">
											<p><?php _e( 'This is the error message in the page.php template.', 'bonestheme' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>

						</div>

						</div>

			</div>

<?php get_footer(); ?>
