<?php get_header(); ?>
			<div id="content">

				<div id="inner-content" class="wrap clearfix">

						<div id="main" class="mbal-centre clearfix" role="main">
							
							<?php 
							
							//$queried_object = get_queried_object();
							//$term_id = $queried_object->term_id;
							
							$tax = $wp_query->get_queried_object();
							
							?>
							
							<h1 class="archive-title"><!--Expositions : --><?php echo $tax->name." ".$term_id; ?> </h1>
							
							<?php if (is_category()) { ?>
								<h1 class="archive-title h2">
									<span><?php _e( 'Posts Categorized:', 'bonestheme' ); ?></span> <?php single_cat_title(); ?>
								</h1>

							<?php } elseif (is_tag()) { ?>
								<h1 class="archive-title h2">
									<span><?php _e( 'Posts Tagged:', 'bonestheme' ); ?></span> <?php single_tag_title(); ?>
								</h1>

							<?php } elseif (is_author()) {
								global $post;
								$author_id = $post->post_author;
							?>
								<h1 class="archive-title h2">
									<span><?php _e( 'Posts By:', 'bonestheme' ); ?></span> <?php the_author_meta('display_name', $author_id); ?>
								</h1>
							<?php } elseif (is_day()) { ?>
								<h1 class="archive-title h2">
									<span><?php _e( 'Daily Archives:', 'bonestheme' ); ?></span> <?php the_time('l, F j, Y'); ?>
								</h1>

							<?php } elseif (is_month()) { ?>
									<h1 class="archive-title h2">
										<span><?php _e( 'Monthly Archives:', 'bonestheme' ); ?></span> <?php the_time('F Y'); ?>
									</h1>

							<?php } elseif (is_year()) { ?>
									<h1 class="archive-title h2">
										<span><?php _e( 'Yearly Archives:', 'bonestheme' ); ?></span> <?php the_time('Y'); ?>
									</h1>
							<?php } ?>

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class( 'clearfix' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

								<header class="article-header">
									<h1 class="page-title">
										<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
										<?php the_title(); ?>
										</a>
									</h1>
								</header>

								<section class="entry-content clearfix" itemprop="articleBody">
									<a href="<?php the_permalink() ?>" rel="bookmark" class="thumbnail-link thumbnail-v1" title="<?php the_title_attribute(); ?>">
										
										<?php
												$the_thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "medium-hard-crop" );
												$the_thumbnail_src2 = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "small-hard-crop" );
												$thumbnail_src = $the_thumbnail_src[0];
												$thumbnail_src2 = $the_thumbnail_src2[0];
												
												//change this to exact WxH of your custom image size
												$check_thumb = strpos($thumbnail_src, '684x452'); 
												$check_thumb2 = strpos($thumbnail_src2, '374x247'); 
													
												// if custom image size exist
												if( $check_thumb ):
													the_post_thumbnail('medium-hard-crop'); 
												elseif( $check_thumb2 ):
													the_post_thumbnail('small-hard-crop'); 
												else:
													echo the_post_thumbnail('thumbnail'); // if not, use default thumbnail
												endif;
											?>
 
 
									</a>
								</section>

								<footer class="article-footer">
									<?php
										//global $post;
										//$startdate = get_post_meta( $post->ID, '_mbal_startdate', true );
										//$enddate = get_post_meta( $post->ID, '_mbal_enddate', true );
										$startdate = get_field('start_date');
										$enddate = get_field('end_date');
									?>
									<h1><a href="<?php the_permalink();?>" rel="bookmark" title="<?php the_title_attribute();?>"><?php echo $startdate; ?></a></h1>
									<h1><a href="<?php the_permalink();?>" rel="bookmark" title="<?php the_title_attribute();?>"><?php echo $enddate; ?></a></h1>
								</footer>

							</article>

							<?php endwhile; ?>

									<?php if ( function_exists( 'bones_page_navi' ) ) { ?>
										<?php bones_page_navi(); ?>
									<?php } else { ?>
										<nav class="wp-prev-next">
											<ul class="clearfix">
												<li class="prev-link"><?php next_posts_link( __( '&laquo; Older Entries', 'bonestheme' )) ?></li>
												<li class="next-link"><?php previous_posts_link( __( 'Newer Entries &raquo;', 'bonestheme' )) ?></li>
											</ul>
										</nav>
									<?php } ?>

							<?php else : ?>

									<article id="post-not-found" class="hentry clearfix">
										<header class="article-header">
											<h1><?php _e( 'No Posts In This Category!', 'bonestheme' ); ?></h1>
										</header>
										<section class="entry-content">
											<p><?php _e( 'Something is missing. Try to using the search box to find the item you are looking for.', 'bonestheme' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the archive.php template.', 'bonestheme' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>

						</div>

					</div>

			</div>

<?php get_footer(); ?>
